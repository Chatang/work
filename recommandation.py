import csv
from sklearn import model_selection
from typing import Tuple

class RecommendationManager:
    def __init__(self,data_file_name,random_state=1,test_size=0.1):
        #self.load_data(data_file_name)
        self.DATA = self.load_data(data_file_name)
        self.TRAINING_DATA, self.TESTING_DATA = self.split_training_and_testing_data(self.DATA,random_state,test_size)
    

    @staticmethod
    def load_data(data_file_name):
        data = [] #or list()
        with open(data_file_name) as data_file:
            csv_reader = csv.reader(data_file)
            for row in csv_reader:
               # print(row)
               data.append(row)
        return data
    
    @staticmethod
    def split_training_and_testing_data(data: list, random_state: float,test_size: float) -> Tuple[list, list]:

        training_data, testing_data = model_selection.train_test_split(data, random_state=random_state, test_size=test_size)
        return (training_data, testing_data)

            
if __name__ == "__main__":
    recommendation_manager = RecommendationManager('store_data.csv')
    print(recommendation_manager.DATA)