class Animal:
    def __init__(self,name,age,legs):
        self.name = name
        self.age = age
        self.legs = legs
        
    def die(self):
        print('I died')
    
class Human(Animal):
    def __init__(self,name,age,legs):
        super().__init__(name,age, 2)
        
    def work(self):
        print('I work')
        
        
if __name__ == "__main__":
    
    